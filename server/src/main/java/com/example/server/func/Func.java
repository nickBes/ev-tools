package com.example.server.func;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
public class Func {
    @Id
    private String id;
    private String name;
    private String fileId;
    private List<String> sheetIds;
}

package com.example.server.sheet;

import com.example.server.field.Field;
import com.example.server.field.FieldRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("sheet")
@CrossOrigin
public class SheetController {
  @Autowired private MongoDatabase db;

  @Autowired private SheetRepository sheetRepository;
  @Autowired private FieldRepository fieldRepository;

  @PostMapping
  public String post(@RequestParam("name") String name) {
    Sheet toRecord = new Sheet();
    toRecord.setName(name);

    Sheet recorded = sheetRepository.insert(toRecord);
    db.createCollection(recorded.getId());

    return recorded.getId();
  }

  @DeleteMapping
  public void delete(@RequestParam("id") String id) {
    sheetRepository.deleteById(id);
    db.getCollection(id).drop();
  }

  @GetMapping
  public List<Sheet> getAll() {
    return sheetRepository.findAll();
  }

  @GetMapping("/{sheetId}/data")
  // returning a string instead of json cause the id is parsed into timestamp
  // when converted to json
  @ResponseBody
  public List<Document> getSheetData(@PathVariable String sheetId) {
    List<Document> documents = new ArrayList<>();
    db.getCollection(sheetId)
        .find()
        .map(
            // parsing the id from timestamp to string
            document -> {
              document.put("id", document.getObjectId("_id").toHexString());
              document.remove("_id");
              return document;
            })
        .forEach(documents::add);

    return documents;
  }

  @PostMapping("/{sheetId}")
  public String createSheetData(@PathVariable String sheetId, @RequestBody JsonNode data) {
    Document dataInBson = Document.parse(data.toString());
    String id = dataInBson.getString("id");
    dataInBson.remove("id");

    MongoCollection<Document> currentCollection = db.getCollection(sheetId);
    List<Field> fields = fieldRepository.findBySheet(sheetId);

    if (id != null) {
      Document update = new Document("$set", dataInBson);
      Document delete = new Document();

      // find removed fields to delete on update
      fields.forEach(
          field -> {
            String fieldName = field.getName();
            if (!dataInBson.containsKey(fieldName)) {
              delete.put(fieldName, "");
            }
          });

      update.put("$unset", delete);

      currentCollection.updateOne(new Document("_id", new ObjectId(id)), update);
    } else {
      id =
          currentCollection
              .insertOne(dataInBson)
              .getInsertedId()
              .asObjectId()
              .getValue()
              .toString();
    }
    return id;
  }

  @DeleteMapping("/{sheetId}")
  public void deleteData(@PathVariable String sheetId, @RequestParam("id") String dataId) {
    db.getCollection(sheetId).deleteOne(new Document("_id", new ObjectId(dataId)));
  }

  @GetMapping("/{sheetId}")
  public Sheet getSheet(@PathVariable String sheetId) {
    return sheetRepository.findById(sheetId).get();
  }
}

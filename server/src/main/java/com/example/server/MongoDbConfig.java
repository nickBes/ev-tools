package com.example.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

@Configuration
public class MongoDbConfig {
  @Value("${spring.data.mongodb.uri}")
  private String dbUri;

  @Value("${spring.data.mongodb.database}")
  private String dbName;

  @Bean
  public MongoClient mongoClient() {
    return MongoClients.create(dbUri);
  }

  @Bean
  public MongoDatabaseFactory mongoDbFactory() {
    return new SimpleMongoClientDatabaseFactory(mongoClient(), dbName);
  }

  @Bean
  public GridFsTemplate gridFsTemplate() {
    DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
    MappingMongoConverter converter =
        new MappingMongoConverter(dbRefResolver, new MongoMappingContext());

    return new GridFsTemplate(mongoDbFactory(), converter);
  }

  @Bean
  public MongoDatabase mongoDatabase() {
    return mongoDbFactory().getMongoDatabase();
  }

}

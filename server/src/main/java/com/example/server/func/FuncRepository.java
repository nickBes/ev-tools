package com.example.server.func;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface FuncRepository extends MongoRepository<Func, String> {}

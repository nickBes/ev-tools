package com.example.server.field;

import com.example.server.sheet.SheetRepository;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("field")
@CrossOrigin
public class FieldController {
  @Autowired private MongoDatabase db;
  @Autowired private SheetRepository sheetRepository;
  @Autowired private FieldRepository fieldRepository;

  @GetMapping("/{sheetId}")
  public List<Field> getAllFields(@PathVariable String sheetId) {
    return fieldRepository.findBySheet(sheetId);
  }

  @PostMapping("/{sheetId}")
  public String createField(@PathVariable String sheetId, @RequestParam("name") String name) {
    Field exists = fieldRepository.findBySheetAndName(sheetId, name);
    if (exists != null) {
      return exists.getId();
    }

    return fieldRepository.insert(new Field(name, sheetId)).getId();
  }

  @DeleteMapping("/{sheetId}")
  public void deleteField(@PathVariable String sheetId, @RequestParam("name") String name) {
    fieldRepository.deleteBySheetAndName(sheetId, name);
    db.getCollection(sheetId)
        .updateMany(new Document(), new Document("$unset", new Document(name, "")));
  }
}

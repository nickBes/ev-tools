package com.example.server.sheet;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Sheet {
  @Id private String id;
  private String name;
}

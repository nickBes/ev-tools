package com.example.server.func;

import com.example.server.sheet.Sheet;
import com.example.server.sheet.SheetController;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/func")
@CrossOrigin
public class FuncController {
  @Autowired private FuncRepository funcRepository;
  @Autowired private GridFsOperations gridFsOperations;
  @Autowired private SheetController sheetController;

  @PostMapping
  public String createFunc(@RequestParam String name) {
    Func toCreate = new Func();
    toCreate.setName(name);

    return funcRepository.insert(toCreate).getId();
  }

  @GetMapping
  public List<Func> getAllFunc() {
    return funcRepository.findAll();
  }

  @DeleteMapping()
  public void delete(@RequestParam String id) {
    Func toDelete = funcRepository.findById(id).get();
    String fileId = toDelete.getFileId();

    if (fileId != null) {
      gridFsOperations.delete(new Query(Criteria.where("_id").is(fileId)));
    }

    funcRepository.deleteById(id);
  }

  @PostMapping("/{funcId}/upload")
  public String upload(@PathVariable String funcId, @RequestParam("file") MultipartFile file) {
    Func current = funcRepository.findById(funcId).get();
    String currentFileId = current.getFileId();

    // if had previous files delete it
    if (currentFileId != null) {
      gridFsOperations.delete(new Query(Criteria.where("_id").is(currentFileId)));
    }

    try {
      String createdFileId =
          gridFsOperations.store(file.getInputStream(), current.getName()).toHexString();
      current.setFileId(createdFileId);
      currentFileId = createdFileId;

      funcRepository.save(current);

      RestTemplate restTemplate = new RestTemplate();
      HttpHeaders headers = new HttpHeaders();
      MultiValueMap<String, Object> request = new LinkedMultiValueMap<>();

      headers.setContentType(MediaType.MULTIPART_FORM_DATA);
      request.add("file", file.getBytes());
      request.add("name", current.getName());

      restTemplate.postForObject(
          "http://127.0.0.1:5000/save", new HttpEntity<>(request, headers), String.class);
    } catch (IOException e) {
      System.out.println("Couldn't get input stream");
    }

    return currentFileId;
  }

  @PostMapping("/{funcId}/sheets")
  public void setSheetIds(
      @PathVariable String funcId, @RequestParam("sheetIds") List<String> sheetIds) {
    Func current = funcRepository.findById(funcId).get();
    current.setSheetIds(sheetIds);

    funcRepository.save(current);
  }

  @PostMapping("/{funcId}/execute")
  public JsonNode execute(@PathVariable String funcId) {
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    Map<String, Object> request = new HashMap<>();
    Map<String, Object> args = new HashMap<>();
    Func current = funcRepository.findById(funcId).get();

    headers.setContentType(MediaType.APPLICATION_JSON);
    request.put("name", current.getName());

    for (String sheetId : current.getSheetIds()) {
      Sheet sheet = sheetController.getSheet(sheetId);
      args.put(sheet.getName(), sheetController.getSheetData(sheetId));
    }

    request.put("args", args);

    return restTemplate.postForObject(
        "http://127.0.0.1:5000/execute", new HttpEntity<>(request, headers), JsonNode.class);
  }

  @GetMapping("/{funcId}")
  public Func getFunc(@PathVariable String funcId) {
    return funcRepository.findById(funcId).get();
  }
}

import ast
from flask import Flask, request


functions = {}
app = Flask(__name__)


@app.post("/save")
def save():
    file = request.form.get("file")
    function_name: str = request.form.get("name")
    print(file)
    ast_object = ast.parse(file)

    for item in ast_object.body:
        if isinstance(item, ast.FunctionDef) and item.name == function_name:
            function_args = item.args.args
            functions[function_name] = (
                ast.Module(item.body, type_ignores=[]),
                function_args[0].arg if len(function_args) > 0 else None,
            )

    return "", 200


@app.post("/execute")
def execute():
    data = request.get_json()
    function_name = data["name"]
    function_args = data["args"]

    func, arg_name = functions[function_name]
    func_locals = {}
    if arg_name != None:
        func_locals[arg_name] = function_args

    exec(compile(func, function_name, "exec"), {}, func_locals)
    response = {}
    response["result"] = func_locals.get("result") or ""

    return response, 200

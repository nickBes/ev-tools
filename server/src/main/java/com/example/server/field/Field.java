package com.example.server.field;

import com.mongodb.lang.NonNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@CompoundIndex(def = "{'name': 1, 'sheetId': 1}", unique = true)
public class Field {
  @Id private String id;
  @NonNull private String name;
  @NonNull private String sheetId;
}

package com.example.server.field;

import org.bson.Document;
import org.springframework.data.mongodb.repository.DeleteQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface FieldRepository extends MongoRepository<Field, String> {
    @Query("{'sheetId': ?0}")
    List<Field> findBySheet(String sheetId);

    @Query("{'sheetId': ?0, 'name': ?1}")
    Field findBySheetAndName(String sheetId, String name);

    @DeleteQuery("{'sheetId': ?0, 'name': ?1}")
    void deleteBySheetAndName(String sheetId, String name);
}

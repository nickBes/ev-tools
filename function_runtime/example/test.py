def avg(data: dict):
    grades: list[dict] = data.get("grades")
    s = 0
    for grade in grades:
        s += int(grade.get("grade"))

    result = s / len(grades)
